
 
# **Apêndice 01 - Aspectos de gerenciamento do projeto**

## **Termo de abertura do projeto**

É o documento que autoriza formalmente o projeto. Ele concede ao gerente
a autoridade para utilizar os recursos da organização na execução das
atividades do projeto.

O termo de abertura do projeto deve abordar, ou referenciar, as
seguintes questões:

-   requisitos que satisfazem as necessidades do cliente

-   objetivos do projeto

-   propósito ou justificação do projeto

-   stakeholders do projeto e os seus papéis e responsabilidades

-   expectativas dos stakeholders

-   identificação do gestor do projeto, e nível de autoridade do gerente

-   cronograma macro dos marcos do projeto

-   premissas, ou pressupostos, organizacionais (fatores considerados
    verdadeiros, reais ou certos)

-   restrições organizacionais (fatores que limitam as opções da equipe)

-   investimento (orçamento preliminar)

-   restrições e riscos

-   descrição do(s) subproduto(s) identificado(s)

-   milestones identificados

-   Permite assim responder a questões como:

-   O que deve ser feito para atingir o objetivo do projeto?

-   Como deve ser feito?

-   Quem que vai fazer?

-   Quando deve ser feito?

## **Lista É / Não É**

Relação do que o produto, ou subproduto, é, e do que o produto, ou
subproduto, não é.

Este processo é necessário para restringir ao seu mínimo o escopo do
projeto, garantindo um melhor foco.

## **Organização da equipe**

Apresentar organograma da equipe de desenvolvimento.

## **Repositórios**

> Apresentar links para acesso aos repositórios do projeto.

## **EAP (Estrutura Analítica de Projeto) Geral do Projeto**

É com base na técnica de decomposição que se consegue dividir os
principais produtos do projeto em nível de detalhe suficiente para
auxiliar a equipe de gerenciamento do projeto na definição das
atividades do projeto.

### **EAP do subsistema 01**

Inserir a EAP de cada um dos subsistemas que compõe o projeto.

### **EAP do subsistema 02**

Inserir a EAP do subsistema 02.

### **Definição de atividades e backlog das sprints**

Definir as atividades gerais que compõe o projeto e elaborar o
cronograma básico de execução. É importante identificar os responsáveis
pelas atividades.
