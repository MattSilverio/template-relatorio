# **Arquitetura geral da solução**

Baseado na compreensão do problema e nos requisitos levantados para o
produto, esta seção apresenta e detalha a arquitetura geral da solução a
ser utilizada no projeto, envolvendo as diversas áreas de conhecimento.

Pode ser elaborado uma representação gráfica da arquitetura geral, para
facilitar a compreensão da proposta.

É importante apresentar aspectos que permitem a integração entre
diferentes áreas.




