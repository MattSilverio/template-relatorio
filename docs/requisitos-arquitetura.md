

# **Concepção e detalhamento da solução**

A seção 02 "Concepção e detalhamento da solução" se refere à fase 02 do
ciclo de vida do projeto e tem por objetivo apresentar a arquitetura da
solução/sistema para o problema.

# **Requisitos gerais**

O time está encarregado de coletar os requisitos -- o processo de
definição e documentação do projeto e das características e funções do
produto necessárias para satisfazer todas as necessidades e expectativas
das partes interessadas. Estes requisitos, colocados numa relação de
requisitos, devem incluir:

-   requisitos de funcionamento e de desempenho;

-   outros requisitos essenciais para projeto e desenvolvimento.

-   Resultados da análise crítica dessas entradas quanto a suficiência,
    completeza e ausência de ambiguidades ou conflitos entre si.

**Tabela 1: Requisitos funcionais para o produto**

| Requisito | Descrição | Observações |
|:---------:|:---------:|:-----------:|
|     1     |           |             |
|     2     |           |             |

**Tabela 1: Requisitos não funcionais para o produto**

| Requisito | Descrição | Observações |
|:---------:|:---------:|:-----------:|
|     1     |           |             |
|     2     |           |             |

